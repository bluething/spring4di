package com.pegipegi;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pegipegi.knights.Knight;
import com.pegipegi.people.Customer;

/**
 * Hello world!
 *
 */
public class App {
	public static void main( String[] args ) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("ConstructorInjection.xml");
		Knight knight = context.getBean(Knight.class);
		knight.embarkOnQuest();
		
		context.close();
		
		ClassPathXmlApplicationContext context2 = new ClassPathXmlApplicationContext("SetterInjection.xml");
		Knight knight2 = context2.getBean(Knight.class);
		knight2.embarkOnQuest();
		
		context2.close();
		
		ClassPathXmlApplicationContext context3 = new ClassPathXmlApplicationContext("AmbiguitiesInjection.xml");
		Customer customer = (Customer) context3.getBean("customerBean");
		System.out.println(customer);
		context3.close();
	}
}

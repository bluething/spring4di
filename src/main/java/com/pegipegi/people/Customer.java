/**
 * 
 */
package com.pegipegi.people;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public class Customer {
	private String name;
	private String address;
	private int age;
	
	public Customer(String name, String address, int age) {
		this.name = name;
		this.address = address;
		this.age = age;
	}
	
	public Customer(String name, int age, String address) {
		this.name = name;
		this.age = age;
		this.address = address;
	}
	
	public String toString(){
		return " name : " +name + "\n address : "
               + address + "\n age : " + age;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	public void setAddress(String address)
	{
		this.address = address;
	}
	public void setAge(int age)
	{
		this.age = age;
	}
	
	
}

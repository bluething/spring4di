/**
 * 
 */
package com.pegipegi.quest;

import java.io.PrintStream;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public class SlayDragonQuest implements Quest {
	
	public SlayDragonQuest(){}
	
	private PrintStream stream;
	
	public SlayDragonQuest(PrintStream stream) {
		this.stream = stream;
	}

	/* (non-Javadoc)
	 * @see com.pegipegi.quest.Quest#embark()
	 */
	@Override
	public void embark() {
		stream.println("Embarking on quest to slay the dragon!");
	}

	public void setStream(PrintStream stream) {
		this.stream = stream;
	}

}

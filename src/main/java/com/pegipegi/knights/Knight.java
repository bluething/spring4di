/**
 * 
 */
package com.pegipegi.knights;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public interface Knight {
	
	public void embarkOnQuest();
	
}

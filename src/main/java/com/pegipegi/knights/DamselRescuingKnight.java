/**
 * 
 */
package com.pegipegi.knights;

import com.pegipegi.quest.Quest;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public class DamselRescuingKnight implements Knight {
	
	public DamselRescuingKnight(){}
	
	private Quest quest;
	
	/**
	 * 
	 */
	public DamselRescuingKnight(Quest quest) {
		this.quest = quest;
	}

	/* (non-Javadoc)
	 * @see com.pegipegi.knights.Knight#embarkOnQuest()
	 */
	@Override
	public void embarkOnQuest() {
		quest.embark();
	}

	public void setQuest(Quest quest) {
		this.quest = quest;
	}

}
